package com.example.exceldemo.listener;

import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.event.AnalysisEventListener;
import com.example.exceldemo.domain.Student;
import com.example.exceldemo.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
public class WebStudentListener extends AnalysisEventListener<Student> {
    @Autowired
    StudentService studentService;
   List<Student> students= new ArrayList<Student>();


    @Override
    public void invoke(Student student, AnalysisContext analysisContext) {
        students.add(student);
//        五个操作一次
        if (students.size()%5==0) {
            studentService.readExcel(students);
            students.clear();
        }
    }

    @Override
    public void doAfterAllAnalysed(AnalysisContext analysisContext) {

    }
}
